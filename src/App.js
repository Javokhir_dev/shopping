import React from 'react'
import { Route, BrowserRouter, Routes } from 'react-router-dom'
import Products from './components/Products'
import Checkout from './components/Checkout'
import Nav from './components/Nav'
import CartDetail from './components/CartDetail'
import { CartProvider } from './CartContext'

const App = () => {
  return (
    <div>
      <CartProvider>
        <BrowserRouter>
          <Nav />
          <Routes>
            <Route path='/' element={<Products />} />
            <Route path='chekout' element={<Checkout />} />
            <Route path='products/:id' element={<CartDetail />} />
          </Routes>
        </BrowserRouter>
      </CartProvider>
    </div>
  )
}

export default App