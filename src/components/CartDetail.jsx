import React, { useContext, useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import './style.css'
import { HiOutlineShoppingCart } from 'react-icons/hi'
import CartContext from '../CartContext';

const CartDetail = () => {
    const {addToCard} = useContext(CartContext)
    const { id } = useParams();
    const [post, setPost] = useState(null);

    useEffect(() => {
        fetch(`https://dummyjson.com/products/${id}`)
            .then((res) => res.json())
            .then((prod) => setPost(prod));
    }, [id]);

    console.log(post);

    return (
        <div>
            
            {
                post && (
                    <div className='container py-4'>
                        <div className="d-flex gap-5">
                            <div className="images">
                                <img className='head_img' src={post.thumbnail} alt="" />
                                <div className='d-flex tow  justify-content-between '>
                                    <img src={post.images[1]} alt="" />
                                    <img src={post.images[2]} alt="" />
                                </div>
                            </div>
                            <div className="products__info w-100">
                                <h2>{post.title}</h2>
                                <div className='circle my-3'></div>
                                <p className='fs-5'>{post.description}</p>
                                <div className=" brands">
                                    <p>Rating: <span>{post.rating}</span></p>
                                    <p>|</p>
                                    <p>Brand: <span>{post.brand}</span></p>
                                    <p>|</p>
                                    <p>Category: <span>{post.category}</span></p>
                                </div>
                                <div className="price">
                                    <h5 className='my-3'>Price: ${post.price}</h5>
                                </div>
                                <button onClick={() =>addToCard({post, id})} className='btn btn-primary py-3 mt-3 fs-5 d-flex align-items-center gap-3'>
                                    <HiOutlineShoppingCart />
                                    <span >Add To Card</span>
                                </button>
                            </div>
                        </div>
                    </div>
                )
            }
        </div>
    )
}

export default CartDetail;
