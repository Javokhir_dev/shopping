import React, { useContext } from 'react'
import { Link } from 'react-router-dom'
import { HiOutlineShoppingCart } from 'react-icons/hi'
import CartContext from '../CartContext'
const Nav = () => {
  const { items } = useContext(CartContext)
  return (
    <div>
      <nav className="navbar bg-body-tertiary py-3 px-4">
        <div className="container-fluid">
          <Link to={"/"} className="navbar-brand">Navbar</Link>
          <Link to={"chekout"}>
            <button className="d-flex btn btn-primary gap-2 align-items-center">
              <HiOutlineShoppingCart />
              <span>{items.length}</span>
            </button>
          </Link>

        </div>
      </nav>
    </div>
  )
}

export default Nav