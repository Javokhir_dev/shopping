import React, { useState, useEffect } from 'react';
import Card from './Card';
import './style.css'

const Products = () => {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true)
  useEffect(() => {
    fetch('https://dummyjson.com/products')
      .then((res) => res.json())
      .then((prod) => {
        setData(prod.products)
        setLoading(false)
      })
      .catch(error => {
        console.log("Error fetch: " + error);
        setLoading(false)
      })
  }, []);

  console.log(data);



  return (
    <div className="container">
      <div className='row '>
        {
          loading ? (<div class=" lds-ellipsis"><div></div><div></div><div></div><div></div></div>) : (
            (data?.map((item, i) => (
              <Card item={item} id={i} />
            )))
          )
        }
      </div>
    </div>
  )
};

export default Products;
